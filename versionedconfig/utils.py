import typing

import semver

from . import migration


def get_migrations(
        base_migration: typing.Type[migration.Migration],
        starting_from_version: typing.Optional[typing.Union[str, semver.Version]] = None,
        to_version: typing.Optional[typing.Union[str, semver.Version]] = None,
) -> typing.List[migration.Migration]:
    if type(starting_from_version) is str:
        starting_from_version = semver.Version.parse(starting_from_version)
    if type(to_version) is str:
        to_version = semver.Version.parse(to_version)
    migrations = [
        x
        for x in filter(
            lambda m: all([
                starting_from_version is None or starting_from_version <= m.get_from_version(),
                to_version is None or m.get_to_version() <= to_version,
            ]),
            [
                m()
                for m in base_migration.__subclasses__()
            ],
        )
    ]
    migrations.sort(
        key=lambda m: [
            m.get_from_version(),
            m.get_to_version(),
        ],
    )
    i = 1
    while i < len(migrations):
        m1 = migrations[i-1]
        m2 = migrations[i]
        if m2.get_to_version() >= m1.get_to_version():
            i2 = 0
            while i2 < i:
                if m2.get_from_version() <= migrations[i2].get_from_version():
                    migrations.pop(i2)
                    i -= 1
                    continue
                i2 += 1
        i += 1
    return migrations
