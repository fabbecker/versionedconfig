import abc

import semver


class Migration(abc.ABC):
    @property
    @abc.abstractmethod
    def from_version(self) -> str:
        return ''

    @property
    @abc.abstractmethod
    def to_version(self) -> str:
        return ''

    def get_from_version(self) -> semver.Version:
        return semver.Version.parse(self.from_version or '0.0.0')

    def get_to_version(self) -> semver.Version:
        return semver.Version.parse(self.to_version or '0.0.0')

    @staticmethod
    def additional_compatibility_check(config):
        return True

    def check_compatibility(self, config):
        if 'version' in config and config['version'] == self.from_version:
            return self.additional_compatibility_check(
                config=config,
            )
        elif self.from_version is None:
            return 'version' not in config
        else:
            return False

    def update_version(self, configuration: dict):
        configuration['version'] = self.to_version
        return configuration

    def convert(
            self,
            configuration: [dict],
    ) -> dict:
        return self.update_version(configuration)
