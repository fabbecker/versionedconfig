import asyncio
import datetime
import inspect
import json
import typing

from . import utils


class Configuration(dict):
    def __init__(
            self,
            data: dict = None,
            path: str = None,
    ):
        self.update(
            data or {},
        )
        self.path = path
        self.run_autosave = False
        super().__init__()

    def read(
            self,
            object_hook: typing.Callable = None,
    ):
        with open(self.path) as file:
            return Configuration(
                data=json.loads(
                    file.read(),
                    object_hook=object_hook,
                ),
                path=self.path,
            )

    def write(
            self,
            indent=None,
            json_encoder: typing.Type[json.JSONEncoder] = None,
    ):
        with open(self.path, 'w') as file:
            file.write(
                json.dumps(
                    self,
                    indent=indent,
                    cls=json_encoder,
                )
            )

    def migrate(
            self,
            migration,
    ):
        if inspect.isclass(migration):
            migrations = utils.get_migrations(
                starting_from_version=self.get('version', '0.0.0'),
                base_migration=migration,
            )
        elif type(migration) is list:
            migrations = migration
        else:
            migrations = [migration]

        for mig in migrations:
            self.update(
                **mig.convert(self),
            )

    async def autosave(
            self,
            interval: [int, datetime.timedelta] = None,
            indent: int = None,
            json_encoder: typing.Type[json.JSONEncoder] = None,
    ):
        if interval is None:
            interval = 60
        if type(interval) is datetime.timedelta:
            interval = interval.total_seconds()
        self.run_autosave = True
        while self.run_autosave and self.path:
            self.write(
                indent=indent,
                json_encoder=json_encoder,
            )
            await asyncio.sleep(interval)

    def __eq__(self, other):
        if type(self) == type(other) and self.__dict__ == other.__dict__:
            return True
        return False
