import json
import unittest as _unittest

import versionedconfig.configuration


class TestStorage(_unittest.TestCase):
    def setUp(self) -> None:
        self.file = 'test_file.json'

    def test_write_and_read(self):
        class CustomClass(object):
            def __init__(
                    self,
                    d,
            ):
                self.data = d

        class CustomEncoder(json.JSONEncoder):
            def default(self, o):
                return {CustomClass.__name__: o.__dict__}

        def decode_custom(dct):
            if type(dct) is dict and list(dct.keys()) == [CustomClass.__name__]:
                obj = CustomClass(d=None)
                obj.__dict__ = dct[CustomClass.__name__]
                return obj
            return dct

        data = versionedconfig.configuration.Configuration(
            {
                'a': {
                    'b': 'c',
                },
                'd': 1,
                'custom object': CustomClass('Hello World!'),
            },
            path=self.file,
        )
        data.write(
            indent=4,
            json_encoder=CustomEncoder,
        )
        new_data = data.read(
            object_hook=decode_custom,
        )

        self.assertEqual(
            data,
            new_data,
            'Data has not been recovered from file',
        )
