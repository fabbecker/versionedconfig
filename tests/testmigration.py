import unittest as _unittest
from abc import ABC


class TestMigration(_unittest.TestCase):
    def test_migrating(self):
        import versionedconfig as vc

        class BaseMigration(
            vc.Migration,
            ABC,
        ):
            """
            """

        class Mig001(BaseMigration):
            from_version = None
            to_version = '0.0.1'

            @staticmethod
            def additional_compatibility_check(config):
                return config == {}

            def convert(self, configuration):
                return self.update_version(configuration)

        # Get all migrations of the current scope
        migration_objects = vc.get_migrations(
            BaseMigration,
        )

        self.assertEqual(
            1,
            len(migration_objects),
            'Number of migration objects does not meet expectations.',
        )

        updated_config = vc.Configuration(
            {},
        )
        updated_config.migrate(
            BaseMigration,
        )
        self.assertIn(
            'version',
            updated_config.keys(),
            'Version attribute does not exist in config',
        )
        self.assertEqual(
            updated_config['version'],
            Mig001.to_version,
            'Version is set to an unexpected value',
        )

        class Mig002(
            BaseMigration,
        ):
            from_version = '0.0.1'
            to_version = '0.0.2'

            @staticmethod
            def additional_compatibility_check(config):
                return config == {}

            def convert(self, configuration):
                return self.update_version(configuration)

        # Get all migrations of the current scope
        migration_objects = vc.get_migrations(
            BaseMigration,
        )

        self.assertEqual(
            2,
            len(migration_objects),
            'Number of migration objects does not meet expectations.',
        )

        updated_config.migrate(BaseMigration)
        self.assertIn(
            'version',
            updated_config.keys(),
            'Version attribute does not exist in config',
        )
        self.assertEqual(
            Mig002.to_version,
            updated_config['version'],
            'Version is set to an unexpected value',
        )

    def test_migrating_from_X_to_Y(self):
        import versionedconfig as vc

        class BaseMigration(
            vc.Migration,
            ABC,
        ):
            """
            """

        class Migration001to002(
            BaseMigration,
        ):
            from_version = '0.0.1'
            to_version = '0.0.2'

            def convert(self, configuration: [dict, vc.Configuration]) -> dict:
                configuration['attribute'] = 'value'
                return self.update_version(
                    configuration=configuration,
                )
        config = vc.Configuration(
            {
                'version': '0.0.1',
            },
        )
        config.migrate(
            BaseMigration,
        )
        self.assertEqual(
            '0.0.2',
            config['version'],
            'Version is not updated',
        )
        config['attribute'] = 'this should not be updated anymore'
        config.migrate(
            BaseMigration,
        )
        self.assertEqual(
            'this should not be updated anymore',
            config['attribute'],
            'Values are updated even though they shouldn\'t',
        )

    def test_big_version_jumps(self):
        import versionedconfig

        migrations_that_have_been_run = list()

        class BaseMigration(
            versionedconfig.Migration,
            ABC,
        ):
            """
            """
            should_run: bool

            def convert(self, configuration: [dict]) -> dict:
                migrations_that_have_been_run.append(type(self).__name__)
                return super().convert(configuration)

        class Mig001To004(
            BaseMigration,
        ):
            from_version = '0.0.1'
            to_version = '0.0.4'
            should_run = True

        class MigTo001(
            BaseMigration,
        ):
            from_version = None
            to_version = '0.0.1'
            should_run = True

        class Mig001To002(
            BaseMigration,
        ):
            from_version = '0.0.1'
            to_version = '0.0.2'
            should_run = False

        expected_results = {
            c.__name__: c.should_run
            for c in BaseMigration.__subclasses__()
        }

        config = versionedconfig.Configuration(
            {}
        )

        config.migrate(
            BaseMigration,
        )

        for migration, result in expected_results.items():
            if result:
                cb = self.assertIn
            else:
                cb = self.assertNotIn
            cb(
                migration,
                migrations_that_have_been_run,
                'migration run status does not meet expectations: %s is not %s' % (
                    migration,
                    result,
                ),
            )

        self.assertEqual(
            '0.0.4',
            config['version'],
        )
