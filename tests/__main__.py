import unittest

from .testmigration import *
from .teststorage import *


if __name__ == '__main__':
    unittest.main()
